import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;
class Card {
    String mName;
    private HashMap<String,String> mDeck = new HashMap<String,String>();
    private HashMap<String,String> mSuite = new HashMap<String,String>();
    public Card(String sPhrase){
        mName = sPhrase;
    }
    public void setHash() {
        mDeck.put("2", "Two");
        mDeck.put("3", "Three");
        mDeck.put("4", "Four");
        mDeck.put("5", "Five");
        mDeck.put("6", "Six");
        mDeck.put("7", "Seven");
        mDeck.put("8", "Eight");
        mDeck.put("9", "Nine");
        mDeck.put("10", "Ten");
        mDeck.put("J", "Jack");
        mDeck.put("Q", "Queen");
        mDeck.put("K", "King");
        mDeck.put("A", "Ace");
        mSuite.put("D", " of Diamond");
        mSuite.put("H", " of Hearts");
        mSuite.put("S", " of Spade");
        mSuite.put("C", " of Clubs");

    }
    public boolean isTrue(String strUserInput, int nIndex) {
        boolean bCheck = false;
        if (nIndex == 0) {
            if (mDeck.containsKey(String.valueOf(strUserInput.charAt(nIndex))) == true) {
                bCheck = true;
                return bCheck;
            }
        }
        else if (nIndex == 1 || nIndex == 2 ) {
            if (mSuite.containsKey(String.valueOf(strUserInput.charAt(nIndex))) == true) {
                bCheck = true ;
                return bCheck;
            }
        }
        return bCheck;
    }
    public String getDescription(String strUserInput){
        String strReturn = "";
        if (strUserInput.charAt(0) == 'D' || strUserInput.charAt(0) == 'C'
                || strUserInput.charAt(0) == 'H' || strUserInput.charAt(0) == 'S'){
            strReturn = "Unknown";
            return strReturn;
        }

        if(strUserInput.charAt(0) == '1') {
            if (strUserInput.charAt(1) == '0') {
                strReturn += "ten";
                if (this.isTrue(strUserInput, 2) == true) {

                    strReturn += mSuite.get(String.valueOf(strUserInput.charAt(2)));
                } else {
                    strReturn = "unknown";
                }
                return strReturn;
            } else {
                strReturn = "unknown";
            }
            return strReturn;
        }
        for(int nIndex = 0; nIndex < strUserInput.length(); nIndex++) {
            if (nIndex == 0) {
                if (this.isTrue(strUserInput,nIndex) == true) {
                    strReturn+= mDeck.get(String.valueOf(strUserInput.charAt(nIndex)));
                }
            } else if (this.isTrue(strUserInput,nIndex) == true) {
                strReturn+= mSuite.get(String.valueOf(strUserInput.charAt(nIndex)));

            } else {
                strReturn = "unknown";

            }
        }
        return strReturn;
    }


}
public class E5_19 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        boolean bRepeat = true;
        String strInput = "";

        while (bRepeat == true) {
            System.out.print("Enter the card notation: ");
            strInput = in.next();
            if(strInput.length() < 2) {
                System.out.println("Enter at least 2 notations ");
            }
            else{
                bRepeat = false;

            }
        }
        strInput = strInput.toUpperCase();
        Card cardC1 = new Card(strInput);
        cardC1.setHash();
        String strCardDescription = cardC1.getDescription(strInput);
        System.out.println(strCardDescription);


    }
}
