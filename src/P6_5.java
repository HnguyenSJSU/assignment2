import java.util.Scanner;
class PrimeGenerator{
    int mPrimeNum;
    public PrimeGenerator(int nTempNum) {
        this.mPrimeNum = nTempNum;
    }

    public void NextPrime() {
        for(int nIndex = 2; nIndex <= this.mPrimeNum; nIndex++){
            if(this.isPrime(nIndex) == true) {
                System.out.print(String.valueOf(nIndex) + " ");
            }
        }
    }

    public boolean isPrime(int i) {
        boolean bPrime = false;
        int nTimes=0;

        for(int nIndex = 2; nIndex <= i; nIndex++) {
            if(i% nIndex == 0) {
                nTimes+=1;
            }
        }
        if(nTimes>1) {
            return bPrime;
        }else {
            bPrime = true;
            return bPrime;
        }

    }

}

public class P6_5 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Please enter a number: ");
        int nUserInput = in.nextInt();
        PrimeGenerator m1 = new PrimeGenerator(nUserInput);
        m1.NextPrime();
        in.close();
    }
}
