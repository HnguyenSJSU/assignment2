import java.util.InputMismatchException;
import java.util.Scanner;
public class E4_17 {
    public static boolean isValid(int nTemp) {
        if (String.valueOf(nTemp).length()>4 || nTemp > 2400) {
            return false;
        }
        else{
            return true;
        }
    }
    public static void main(String[] args) {
        int nFirstTime = 0, nSecondTime = 0;
        String sFirst = "";
        Scanner in = new Scanner(System.in);
        boolean bRepeat = true;

        while (bRepeat == true) {
            try {
                System.out.print("Please enter the first time: ");
                nFirstTime = in.nextInt();
                System.out.print("Please enter the second time: ");
                nSecondTime = in.nextInt();
                if(!isValid(nFirstTime) || !isValid(nSecondTime)) {
                    System.out.println("Try again" );
                    continue;
                }
                bRepeat = false;

            } catch (InputMismatchException e) {
                System.out.println("Please enter 4 digits");
                in.nextLine();
            }
        }

        //turn all values into minutes
        int nFirstMin = (nFirstTime/100)*(60)+(nFirstTime % 100);
        int nSecondMin = (nSecondTime/100)*(60)+(nSecondTime % 100);

        if (nSecondMin < nFirstMin){
            //add 24 hrs into second total minutes
            nSecondMin += 1440;
        }
        int nDiff = nFirstMin-nSecondMin;

        if (nDiff < 0 ) {
            nDiff = -(nDiff);
        }
        System.out.print(nDiff/60 +" hours " + nDiff%60 +" minutes");
        in.close();
    }

}


